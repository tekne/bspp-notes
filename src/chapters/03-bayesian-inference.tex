%! Author = marcelroed
%! Date = 11/19/20

% Preamble
\documentclass[../main.tex]{subfiles}

% Document
\begin{document}
    \chapter{Bayesian Inference}

    \section{Problems with Bayesian Inference}

    From a mathematical point of view, Bayesian inference may seem straigtforwards: as we have
    \begin{equation}
        \Pr[\theta | \mc{D}] \propto \Pr[\mc{D} | \theta]\Pr[\theta]
    \end{equation}
    we already know the posterior probability of any value of \(\theta\) \textit{relative} to another. In practice, however, Bayesian inference poses a major computational challenge. This is because, for non-trivial models, Bayesian inference amounts to computing a high-dimensional integral, namely the normalising constant
    \begin{equation}
        \int_\Omega p(\mc{D} | \theta)p(\theta)d\theta
        \label{eqn:explike}
    \end{equation}
    In fact, Bayesian inference of graphical models, which are a specific class of probabilistic models, is an NP-hard problem: the models expressible by a Turing-complete probabilistic programming language are strictly more general than graphical models.

    We can identify two challenges of Bayesian inference:
    \begin{enumerate}

        \item Computing the \textbf{normalizing constant}, or more generally the \textbf{expectation} of some function \(f(\theta)\) over the posterior distribution, given by
        \begin{equation}
            \Ex_{p(\theta | \mc{D})}[f(\theta)] = \int f(\theta)p(\theta | \mc{D})d\theta
        \end{equation}

        \item Characterizing the posterior distribution, typically by providing a set of (approximate) \textbf{samples}.

    \end{enumerate}

    Each of the two problems presents a distinct challenge: many methods sidestep the first and directly produce approximate samples.

    The integral in equation \ref{eqn:explike} can be viewed the expectation of the likelihood function under the prior distribution, hence the term \textbf{marginal likelihood}. In general (as is the case with integrals), \(p(\mc{D})\) has no closed-form solution. High-dimensional integrals are hard to approximate, and hence, it is difficult to get a good estimate for \(p(\mc{D})\). The issue with this is that, when we don't know \(p(\mc{D})\), we lack scaling when evaluating a point: that is, we have no clue how significant that point is compared to the distribution as a whole. This is because we do not know whether there are other substantially more probable events that we may have missed so far, which would in term imply that the point in question has a negligible chance of occuring.

    In fact, the larger the parameter space, the more difficult this become. However, if we do manage to compute the normalizing constant, then we may compue the posterior exactly via an application of Bayes' rule.

    \subsubsection{Example: without marginal likelihood, we lack scaling}

    Consider a model with possible parameters \(\theta \in \{1, 2, 3\}\) having uniform prior \(p(\theta) = \frac{1}{3}\). Suppose we are only able to evaluate
    \begin{equation}
        p(\mc{D} | \theta = 1) = 1, \qquad P(\mc{D} | \theta = 2) = 10
    \end{equation}
    Recall that marginal likelihood is given in this discrete case by the summation
    \begin{equation}
        p(\mc{D}) = \sum_{\theta \in \{1, 2, 3\}}p(\mc{D} | \theta)p(\theta)
    \end{equation}
    Depending on what exactly \(p(\mc{D})\) is, the posterior probability of say \(p(\theta = 2 | \mc{D})\) will vary wildly:
    \begin{itemize}

        \item If \(p(\mc{D} | \theta = 3) = 1\), we have \(p(\mc{D}) = 10\) and hence \(p(\theta = 2 | \mc{D}) = 5/6\)

        \item If \(p(\mc{D} | \theta = 3) = 2989\), we have \(p(\mc{D}) = 1000\) and hence \(p(\theta = 2 | \mc{D}) = 1/300\).

    \end{itemize}
    Hence, if we have no way of knowing the posterior, we can only know the relative scales of the priors, but not their absolute scales! It gets worse though:

    \subsection{Example: knowing the normalizing constant is not enough}

    \label{ex:notenough}

    We have just seen that not knowing the normalizing constant is very bad. Unfortunately, knowing the normalizing constant is actually not enough, in the sense that it does not necessarily allow us to solve all the inference problems we may be interested in. Consider the following inference problem as an example:
    \begin{itemize}

        \item The \textbf{prior} on \(\theta\) is distributed according to a Gamma distribution with shape parameter 3 and rate parameter 1, i.e.
        \begin{equation}
            \forall \theta \in (0, \infty), p(\theta) = \Gamma(\theta | 3, 1) = \frac{\theta^2e^{-\theta}}{2}
        \end{equation}

        \item The \textbf{likelihood function} is a student-t distribution with degree of freedom 2 on the difference between \(\theta\) and the output 5, i.e.
        \begin{equation}
            p(y = 5 | \theta) = \Stt(\theta - 5 | 2) = \frac{\Gamma(1.5)}{2\pi}\left(
            1 + \frac{(\theta-  5)^2}{2}
            \right)^{-3/2}
        \end{equation}
        The student-t distribution, or simply the t distribution, is any member of a family of distributions arising when estimating the mean of a normally distributed population, usually when the sample size is small and the population standard deviation is unknown.

        The t-distribution was discovered by William Sealy Gosset in 1908. His employers, Guinness breweries, required him to publish under a pseudonym, so he went by "Student", hence the name.

        \item For the \textbf{posterior}, using numerical integration over \(\theta\), we may compute the normalizing constant quite accurately, giving this closed-form expression for the posterior:
        \begin{equation}
            p(\theta | y = 5) = 5.348556\theta^2e^{-\theta}(2 + (5 - \theta)^2)^{-3/2}
            \label{eqn:nspost}
        \end{equation}

    \end{itemize}
    In Figure \ref{fig:exnorm}, we plot the above. Now, since the posterior (\ref{eqn:nspost}) is not a standard distribution, we can't sample from it without further calculations. For example, if we know the inverse of the cdf of the posterior,
    \begin{equation}
        P(\Theta \leq \theta | y = 5) = \int_0^{\theta}p(t | y = 5)d\Theta
    \end{equation}
    we can sample from the posterior by sampling from the uniform distribution over \((0, 1)\) and then apply the inverse of the cdf to that sample. Unfortunately, both the cdf and it's inverse cannot be calculated analytically in general. This is a simple one-dimensional problem: in this case the cdf of this distribution and it's inverse can actually easily be estimated numerically, however, numerical approximation of this kind can be extremely difficult for most problems where the parameter \(\theta\) has more than a few dimensions.

    \begin{figure}
        \centering
        \begin{tikzpicture}
            \begin{axis}[samples=500, domain=0:15]
                \addplot[color=blue]{x^2 * exp(-x) / 2};
                \addplot[color=red]{
                0.353553 * (1 + ((x - 5)^2)/2)^(-1.5)
                };
                \addplot[color=purple]{
                5.348556 * x^2 * exp(-x) * (2 + (5 - x)^2)^(-1.5)
                };
            \end{axis}
        \end{tikzpicture}
        \caption{
        \(p(\theta)\) (blue), \(p(y = 5 | \theta)\) (red), \(p(\theta | y = 5)\) (purple) as in Example \ref{ex:notenough}
        }
        \label{fig:exnorm}
    \end{figure}

    Similarly, if we wish to estimate an expectation with respect to a posterior, we may do this rather easily numerically for this simple problem, for example using Simpson's rule, but in higher dimensions this will be impractical. There are a number of indirect methods we may use in general to sample from the posterior, such as rejection sampling, importance sampling, and MCMC, however, as we will see, later in the course, these all only require that we can evaluate an \textit{unnormalized} version of the probability distribution. This allows us to sidestep the need to calculate the marginal likelihood i.e. normalizing constant.

    \subsection{Why is sampling hard?}

    So let's consider the subject of sampling from a distribution generally. Suppose we are interested in the pdf \(p(\theta)\). Typically, we can only evaluate \(p(\theta)\) to within a multiplicative constant (the normalizing constant), i.e. we can evaluate \(p^*(\theta)\) with
    \begin{equation}
        p(\theta) = p^*(\theta) / Z, \qquad Z = \int d\theta p^*(\theta)
    \end{equation}
    We have two difficulties:
    \begin{itemize}
        \item Normally, we don't know the normalizing constant \(Z\)
        \item Even if we do know \(Z\), it is still challenging to sample from \(p(\theta)\), especially in high-dimensional spaces.
    \end{itemize}
    Correct samples, after all, will by definition tend to come from the areas in \(\theta\)-space where \(p(\theta)\) is large. The question, then, is how to identify these places without evaluating the probability density everywhere.

    Let's consider an analogy due to MacKay. Suppose we want to draw random water samples from a lake and find average plankton concentration. Let us say the depth of the lake at \(\mb{x} = (x, y\)) is \(p^*(\mb{x})\), and let us assume the plankton concentration is a function of \(\mb{x}\), \(f(\mb{x})\).

    To calculate the average plankton concentration, we first calculate the total plankton population of the lake,
    \begin{equation}
        P = \int f(\mb{x})p^*(\mb{x})d\mb{x}
    \end{equation}
    We then divide that by the volume of the lake,
    \begin{equation}
        \Phi = \frac{P}{Z}, \qquad Z = \int p^*(\mb{x})d\mb{x}
        \label{eqn:aplankton}
    \end{equation}

    Now, suppose we are provided with a boat, GPS, and a plumbline. Using the GPS, we may use the boat to reach any desired location \(\mb{x}\) on the map, and using the plumbline we may measure \(p^*(x)\) at \(\mb{x}\) as well as the plankton concentration at \(\mb{x}\). We are asked to solve two problems:
    \begin{itemize}
        \item Problem 1: find the average plankton concentration \(\Phi\) from Equation \ref{eqn:aplankton}.
        \item Problem 2: randomly draw water samples of 1 milliliter such that each sample is equally likely to come from any point in the lake.
    \end{itemize}
    These problems are difficult simply because we don't know anything about \(p^*(x)\) at the outset. It is possible that much of the volume of the lake is contained in deep and narrow underwater canyons, in which case to correctly sample from the lake and estimate average plankton concentration, we must implicitly discover the canyons and find their volume relative to the rest of the lake. These are difficult problems, but nevertheless we'll see that the clever Monte Carlo methods can solve them.

    \subsubsection{Knowing the posterior exactly is not enough}

    In practice, having an exact form of the posterior \(p(\theta | \mc{D})\) is still not enough for many tasks we may want to perform, this being especially pronounced when \(\theta\) is continuous or discrete but with a very large number of values. Computing the posterior probability in Bayesian ML is often not the end itself, but a means to an end

    There are in fact many uses of the posterior, such as
    \begin{itemize}

        \item Calculating the population probability or density for some particular instances of \(\theta\)

        \item Calculating the expected value of some function \(\Ex_{p(\theta | \mc{D})}(f(\theta))\), including \(\theta\) itself, in which case we have \(f = \id\).

        \item Making predictions using a \textbf{posterior predictive distribution}, which we will introuce shortly.

        \item Estimate the \textbf{most probable value} of the posterior distribution,
        \begin{equation}
            \theta^* = \argmax_\theta p(\theta | \mc{D})
        \end{equation}
        This is called \textbf{maximum a posteriori (MAP) estimation}.

        \item Construct a useful representation of the posterior not for it's own sake put for passing on to another part of a \textbf{computational pipeline}, or to be directly observed by a user.

    \end{itemize}
    Knowing the normalizing constant \(p(\mc{D})\) is only sufficient for 1: uses 2-5 require more computation. Knowing \(p(\mc{D})\), on the other hand, is neither sufficient nor necessary for drawing samples from the posterior.

    \section{Deterministic Approximation}

    \subsection{Posterior predictive distribution}

    In Bayesian ML, we build a model in the form of a joint probability distribution of the latent variables or parameters and the observed variables, implying a predictive model. One such predictive model is the \textbf{posterior predictive distribution}: the distribution of unseen or new data conditional on observed data. We use the posterior to construct the posterior predictive distribution. To define the posterior predictive distribution, we:
    \begin{itemize}

        \item Introduce a predictive model for new data \(\mc{D}^*\) given \(\theta\) or \(\mc{D}\), \(p(\mc{D}^* | \theta, \mc{D})\).

        \item Margingalize over the posterior distribution, or equivalently, take the expectation of this random variable over the posterior distribution, thus

        \item Obtaining a \textbf{likelihood term} for \(\mc{D}^*\)
        \begin{equation}
            p(\mc{D}^* | \mc{D})
            = \Ex_{p(\theta | \mc{D})}[p(\mc{D}^* | \theta, \mc{D})]
            = \int p(\mc{D}^* | \theta, \mc{D})p(\theta | \mc{D})d\theta
            = \int p(\mc{D}^*, \theta | \mc{D})d\theta`
            \label{eqn:postpred}
        \end{equation}
        The above equation follows from the product rule.

    \end{itemize}
    It is quite common to make the following assumption: the \textbf{conditional independence of data given parameter \(\theta\)}. This amounts to the assumption that the old data \(\mc{D}\) and the new data \(\mc{D}^*\) are conditionally independent given \(\theta\), i.e.
    \begin{equation}
        p(\mc{D}^* | \theta, \mc{D}) = p(\mc{D}^* | \theta)
        \label{eqn:condind}
    \end{equation}
    This amounts to assuming that all relevant information about or model's prediction is encapsulated in \(\theta\).

    There is another predictive model, the \textbf{prior predictive distribution}, which is similar to the posterior predictive distribution except that the marginalization or equivalently the expectation is taken with respect to the prior distribution instead of the posterior distribution.

    \subsection{Coin tossing, revisited}

    Recall that earlier, in Example \ref{sssec:cointoss}, we considered the experiment of tossing a biased coin multiple times and observing the number of heads, our aim being to estimate the bias of the coin, the parameter \(\theta\). Here, our aim is different: to infer a future outcome, i.e. to make predictions.

    Recall the experiment of coin tossing:
    \begin{itemize}

        \item We assume that the coin is biased, with parameter \(\theta\) determining the probability of whether the coin will return heads. We denote the number of heads in \(n\) coin tosses as \(x\).

        \item We have prior \(p(\theta) = \Beta(\theta | \alpha_0, \beta_0)\).

        \item We have likelihood function \(p(x | \theta) = \Bin(x | n, 0)\).

        \item We have posterior \(p(\theta | x) = \Beta(\theta | \alpha_0 + x, \beta_0 + n - x)\).

    \end{itemize}
    Consider now the problem of predicting the probability of heads in a single future trial under the posterior pdf \(\Beta(\theta | \alpha, \beta)\). We have just seen the definition of the posterior predictive distribution, as given by equation \ref{eqn:postpred} (assuming the conditional independence of the data given the parameter \(\theta\), i.e. \ref{eqn:condind}). Therefore, the probability of getting heads in the next throw is given by
    \begin{equation}
        p(x^* = 1 | \mc{D})
        = \int_0^1p(x^* = 1 | \theta)p(\theta | \mc{D})d\theta
        = \int_0^1\theta\Beta(\theta | \alpha, \beta)d\theta
        = \frac{\alpha}{\alpha + \beta}
    \end{equation}
    The integral above expresses the expected value of the \(\Beta\) distribution with parameters \(\alpha, \beta\), which we know to be equal to \(\frac{\alpha}{\alpha + \beta}\).

    \subsection{Estimating the Posterior}

    In this section we consider deterministic methods of approximating the posterior. The Bayesian probabilistic prescription tells us how to reason about models and their parameters, but is often impractical for realistic models outside of the exponential family. There is a simple approach to posterior inference, and that is to ignore the posterior computation (of the normalizing constant) and instead use a heuristic approximation.

    We consider some deterministic approximations of the posterior:
    \begin{itemize}
        \item Point estimation, including:
        \begin{itemize}
            \item Maximum likelihood (ML) estimation
            \item Maximum a posterior (MAP) estimation
        \end{itemize}
        \item Laplace approximation
    \end{itemize}

    First, we will consider point estimation: the idea is to take a point \(\tilde\theta\) of \(\theta\) and then approximate the posterior predictive distribution using only this point estimate:
    \begin{equation}
        p(\mc{D}^* | \mc{D}) \approx p(\mc{D}^* | \tilde\theta)
    \end{equation}
    There are two common methods to choose \(\tilde\theta\): ML estimate and MAP estimate. Computing \(\tilde\theta\) in either case only requires solving an optimization problem, which is generally easier than computing a high-dimensional integral.

    \subsection{Maximum likelihood (ML) estimation}

    ML estimation is a non-Bayesian approach to point estimation: there is no consideration of prior over the parameters. ML estimation computes a parameter value \(\theta^*_{ML}\) that maximimizes the likelihood function
    \begin{equation}
        \theta^*_{ML} = \argmax_{\theta}p(\mc{D} | \theta)
    \end{equation}
    This method is simple; the problem is that ML estimation is prone to overfitting, and by not incorporating prior information, ML estimation can lead to various issues relating to the limitations of the Frequentist approach.

    Let us consider the coin tossing expetriment again. We approximate the posterior predictive distribution by simply choosing a point estimate, taken to be the maximum likelihood estimate: hence the posterior predictive distribution is just the Bernoulli distribution, as in
    \begin{equation}
        p(x^* = 1 | \mc{D}) \approx p(x^* = 1 | \tilde\theta) = \Bern(x^* = 1 | \theta^*_{ML})
    \end{equation}
    Suppose, now, we have seen 3 tails in a row. Then \(\theta^*_{ML} = 0/3 = 0\) since this value makes the observed data maximally probable. Using this, we predict that heads are impossible: this is called the \textbf{zero count problem}, or \textbf{spare data problem}. A particular author would describe this as a "Black Swan".

    \subsection{Maximum a posteriori probability (MAP) estimation}

    The MAP estimate is just the \textbf{mode} of the posterior distribution, i.e.
    \begin{equation}
        \theta^*_{MAP} = \argmax_{\theta}p(\theta | \mc{D}) = \argmax_{\theta}p(\mc{D} | \theta)p(\theta) = \argmax_{\theta}(\log p(\mc{D} | \theta) + \log p(\theta))
    \end{equation}
    The above equalities follow first from Bayes' rule and then from the fact that the \(\log\) function is monotonically increasing.

    MAP estimation is in fact closely related to ML estimation, but there is an important difference, which is that the MAP estimation uses an enhanced objection function by incorporating prior knowledge. In this way, MAP estimation can be seen as a \textbf{regularization} of MLE. In machine learning regularization is the process of adding information in order to prevent overfitting. In many cases, the MAP estimate converges to the ML estimate as more data is observed (assuming that the prior remains unchanged), this phenomenon being known as the data ``overwhelming'' the prior.

    Let's, again, revisit the coin tossing experiment: suppose we have observed \(x\) heads in \(n\) tosses, and assume the prior is \(\Beta(\theta | 1, 1)\), which is equivalent to the uniform distribution. Then the posterior is
    \begin{equation}
        \Beta(\theta | x + 1, n - x + 1)
    \end{equation}
    Taking \(\tilde\theta\) to be the \textbf{posterior mean} we obtain \textbf{Laplace's rule of succession} for the case of coin tossing
    \begin{equation}
        p(x^* = 1 | \mc{D}) \approx p(x^* = 1 | \tilde\theta) = \frac{x + 1}{n + 2}
    \end{equation}
    This says if before we observed any coin toss we thought that all values of the bias \(\theta\) were equally likely, then after observing \(x\) heads out of \(n\) throws, a good estimate of the bias \(\theta\) is \(\frac{x + 1}{n + 2}\). Now, if we instead take \(\tilde\theta\) to be the MAP, i.e. the posterior mode, which in this case is \(\frac{x}{n}\), which in fact equals the maximum likelihood estimate, then the posterior predictive distribution is
    \begin{equation}
        p(x^* = 1 | \tilde\theta) = \frac{x}{n}
    \end{equation}
    Note that
    \(\Beta(\theta | \alpha, \beta)\) has mode \(\frac{\alpha - 1}{\alpha + \beta + 2}\) and mean \(\frac{\alpha}{\alpha + \beta}\).

    \subsection{Laplace approximation}

    Finally, let's look at the Laplace approximation. The Laplace approximation is a much more sophisticated form of aporoximating the posterior. The Laplace approximation refines the MAP estimate by approximating the full posterior with a normal distribution centered on the MAP estimate, but it's covariance (here we are consiering the general multivariate situation) is determined by the local curvature of the log density around this point.

    Formally, the Laplace approximation is given by this normal distribution
    \begin{equation}
        p(\theta | \mc{D}) \approx \Norm(\theta | \theta_{MAP}, (\Lambda_{MAP})^{-1})
    \end{equation}
    whose mean is the MAP estimate and whose covariance is the inverse of the negative Hessian of the log joint density evaluated at the MAP estimate, that is,
    \begin{equation}
        \Lambda_{MAP} = -\nabla^2_\theta\log p(\theta, \mc{D})|_{\theta = \theta^*_{MAP}}
    \end{equation}

    \section{Monte Carlo Methods}

    Monte Carlo methods, or Monte Carlo simulation, is the generation of random objects or processes by means of a computer. These objects could arise naturally, for example when modeling real-life systems, or they could be constructed by design to solve purely deterministic problems which involves random sampling from certain probability distribution. Monte Carlo simulation involves repeating the experiment many times to obtain quantities of interest, using the Laws of Large Numbers and other methods of statistical inference.

    Monte Carlo methods are widely used in much of science, engineering, and finance. Some typical uses include
    \begin{itemize}

        \item In Bayesian statistics, the Markov Chain Monte Carlo or MCMC algorithm is often used to sample from a posterior distribution.

        \item High-dimensional integrals can be evaluated using Monte Carlo methods by writing the integral as the expectation of an appropriate random variable.

        \item Monte Carlo simulation works because randomness allows an efficient search of the function's domain. Hence, Monte Carlo methods can be used for the optimization of complicated deterministic objective functions.

    \end{itemize}

    \subsection{Expectation and variance review}

    Let \(X, Y\) and \(X_1,...,X_n\) be random variables and \(\lambda, \mu\) be constants. We restate the following basic properties
    \begin{align}
        \Ex[X + \mu]                       & = \Ex[X] + \mu                                                             \\
        \Var[X + \mu]                      & = \Var[X]                                                                  \\
        \Ex[\lambda X]                     & = \lambda\Ex[X]                                                            \\
        \Var[\lambda X]                    & = \lambda^2\Var[X]                                                         \\
        \Ex\left[\sum_{i = 1}^nX_i\right]  & = \sum_{i = 1}^n\Ex[X_i]                                                   \\
        \Var\left[\sum_{i = 1}^nX_i\right] & = \sum_{i = 1}^n\Var[X_i] + 2\sum_{i < j}\Cov[X_i, X_j] \label{fig:sumvar}
    \end{align}
    where
    \begin{align}
        \Var[X]    & = \Ex[(X - \Ex[X])^2] = \Ex[X^2] - \Ex[X]^2 \\
        \Cov[X, Y] & = \Ex[(X - \Ex[X])(Y - \Ex[Y])]
    \end{align}
    If \(f(X)\) and \(g(Y)\) are \textbf{independent} random variables, we have
    \begin{equation}
        \Ex[f(X)g(Y)] = \Ex[f(X)]\Ex[g(Y)] \implies \Cov[X, Y] = \Ex[X - \Ex[X]]\Ex[Y - \Ex[Y]] = 0
    \end{equation}
    It follows from equation \ref{fig:sumvar} that
    \begin{equation}
        \Var[X + Y] = \Var[X] + \Var[Y]
    \end{equation}
    Extending this result to \textbf{independently and identically distributed} (iid) random variables \(X_1,...,X_n\) using (\ref{fig:sumvar}), we obtain
    \begin{equation}
        \Var\left[\sum_{n = 1}^NX_n\right] = \sum_{n = 1}^N\Var[X_n] = N\Var[X_1]
    \end{equation}
    Hence, we have
    \begin{equation}
        \Var\left[\frac{1}{N}\sum_{n = 1}^NX_n\right]
        = \frac{1}{N^2}\Var\left[\sum_{n = 1}^NX_n\right]
        = \frac{1}{N}\Var[X_1]
    \end{equation}

    Note that, measure-theoretically, a family of events \(\{A_i | i \in \mc{I}\}\) is said to be \textbf{independent} if for all finite subsets \(\mc{J} \subset \mc{I}\), we have
    \begin{equation}
        \Pr\left[\bigcap_{i \in \mc{J}}A_i\right] = \prod_{i \in J}\Pr[A_i]
        \label{eqn:indmeasure}
    \end{equation}
    Real-valued random variables \(X_1,...,X_n\) are then said to be independent if for all Borel subsets \(B_1,...,B_n \subset \reals\) the events \(\{X_i \in B_i\}\) are independent by the definition in equation \ref{eqn:indmeasure}.

    \subsection{The (Simple) Monte Carlo estimator}

    Assume we are given a function \(f(\theta)\) and \(\theta\) is a random variable under the distribution \(\theta \sim \pi(\theta)\). We are interested in the expectation under the distribution \(\pi\),
    \begin{equation}
        I = \Ex_{\pi(\theta)}[f(\theta)] = \int_\Omega f(\theta)\pi(\theta)d\theta
    \end{equation}
    \begin{definition}[(Simple) Monte Carlo (SMC) Estimator \(\hat I_N\) of \(I\)]
        We define the \textbf{(Simple) Monte Carlo Estimator} \(\hat I_N\) of \(I\) to be the average value of \(f\) on \(N\) indepenent draws from the distribution \(\pi(\theta)\), that is,
        letting \(\theta^{(1)},...,\theta^{(N)}\) be independent and identically distributed random variables according to \(\pi(\theta)\), we define
        \begin{equation}
            \hat I_N = \frac{1}{N}\sum_{i = 1}^Nf(\theta^{(i)})
            \label{eqn:smc}
        \end{equation}
    \end{definition}
    The Simple Monte Carlo estimator is often called the Crude Monte Carlo estimator to distinguish it from other more sophisticated methods and estimators. Despite these mildly pejorative names, Simple Monte Carlo is often the appropriate method. Typically, \(\theta\) ranges over some subset of the Euclidean space, and our function \(f\) is a real valued function. Generally, \(\theta\) ranges over a measurable set over which a probability distribution could be defined, allowing, e.g., \(\theta\) to be the path taken by a wandering particle or an image. So long as the value of \(f\) is something which can be averaged, like a real number or vector, we may apply Simple Monte Carlo.

    \subsection{Monte Carlo estimator is unbiased}

    Let's look at an important property of the SMC estimator: it's unbiasedness. First, suppose we are given iid random variables \(\theta^{(1)},...,\theta^{(n)}\). Then, the SMC estimator given in Equation \ref{eqn:smc} is itself a random variable having it's own mean and variance. Let's look at the mean of an SMC estimator \(\hat I_N\). We have
    \begin{equation}
        \Ex[\hat I_N]
        = \Ex\left[\frac{1}{N}\sum_{n = 1}^Nf(\theta^{(n)})\right]
        = \frac{1}{N}\sum_{n = 1}^N\Ex[f(\theta^{(n)})]
        = \frac{1}{N}\sum_{n = 1}^N\Ex[f(\theta^{(1)})]
        = I
    \end{equation}
    We hence say that the estimator is \textbf{unbiased} (for fixed \(N\)), i.e.
    \begin{equation}
        \Ex[\hat I_N] = I = \Ex[\pi(\theta)]f(\theta)
    \end{equation}
    That is, SMC does not introduce any \textbf{bias}, that is, systematic under or over-estimation, into the approximation.

    \subsection{Variance and error}

    Now, let's consider the variance and error of the SMC estimator. We assume \(\theta^{(1)},...,\theta^{(n)}\) are iid. random variables.

    \begin{lemma}
        Assume \(I = \Ex[f(\theta)]\) and \(\Var[f(\theta)]\) exist. Then
        \begin{equation}
            \Var[\hat I_N] = \Ex[(\hat I_N - I)^2] = \frac{1}{N}\Var[f(\theta)]
        \end{equation}
        \label{lem:varsmc}
    \end{lemma}
    Therefore, the lemma tells us that the estimation gets worse with increased variance of \(f(\theta)\) but better with increased sample size \(N\).

    We define the \textbf{root mean squared error (RMSE)} of \(\hat I_N\) to be
    \begin{equation}
        \sqrt{\Ex[(\hat I_N - I)^2]} \in \mc{O}(1/\sqrt{N})
    \end{equation}
    with the big-\(\mc{O}\) bound following from Lemma \ref{lem:varsmc}.

    \subsection{Weak and strong laws of large numbers}

    Let's review the laws of large numbers before presenting another important statistical property of the SMC estimator, namely that it is consistent. Suppose \(X^{(1)},X^{(2)},...\) are iid. random variables on some fixed probability space \((\Omega, \mc{F}, \Pr)\) with mean \(\mu\).

    \begin{theorem}[Weak law of large numbers]
        \(\frac{1}{N}\sum_{i = 1}^NX^{(i)}\) converges in probability to \(\mu\) as \(N \to \infty\), i.e. for any fixed \(\epsilon > 0\),
        \begin{equation}
            \lim_{n \to \infty}\Pr\left[\left\{
            \omega : \left|\frac{1}{N}\sum_{i = 1}^NX^{(i)}(\omega) - \mu\right| \leq \epsilon
            \right\}\right] = 1
        \end{equation}
    \end{theorem}

    The weak law of large numbers says that the average of \(N\) iid random variables converges in probability to the mean as \(N \to \infty\). Converges in probability means that for any error \(\epsilon > 0\), however small, the chance of missing by more than \(\epsilon\) goes to 0 as \(N \to \infty\).

    \begin{theorem}[Strong law of large numbers]
        \(\frac{1}{N}\sum_{i = 1}^NX^{(i)}\) converges to \(\mu\) almost surely as \(N \to \infty\), i.e.
        \begin{equation}
            \Pr\left[\left\{
            \omega: \lim_{N \to \infty}\frac{1}{N}\sum_{i = 1}^NX^{(i)}(\omega) = \mu
            \right\}\right] = 1
        \end{equation}
    \end{theorem}

    The strong law of large numbers says that the average of \(N\) iid random variables converges with probability 1 to the mean as \(N \to \infty\). Equivalently, it says that the event that the average of \(N\) iid random variables converges to the mean has probability 1.

    \subsection{Monte Carlo estimator is consistent}

    An important property of the SMC estimator is that it is \textbf{consistent}. The weak (and strong) consistency of the SMC estimator is an immediate consequence of applying the weak (or respectively, strong) Law of Large Numbers to the iid random variables \(f(X^{(i)})\). These laws are applicable here since the expectation \(\Ex[f(X)]\) is assumed to exist,

    \begin{theorem}[Weak consistency of the SMC estimator]
        Under the weak law of large numbers, \(\hat I_N\) converges in probability to \(I\) as \(N \to \infty\), i.e. for any \(\epsilon > 0\)
        \begin{equation}
            \lim_{n \to \infty}\pi(|\hat I_N - I| \leq \epsilon) = 1
        \end{equation}
        In other words, the weak law of large numbers says that the chance of missing by more than \(\epsilon\) goes to zero.
    \end{theorem}

    \begin{theorem}[Strong consistency of the SMC estimator]
        Under the strong law of large numbers, \(\hat I_N\) converges almost surely to \(I\) as \(N \to \infty\), i.e.
        \begin{equation}
            \pi\left(\lim_{N \to \infty}\hat I_N = I\right) = 1
        \end{equation}
    \end{theorem}

    While the laws say that the SMC method will eventually prouce an error which is arbitrarily small, neither of us tells us how large \(N\) must be for this to happen, nor do they say whether for a given sample \(\theta^{(1)},...,\theta^{(n)}\) whether the error is likely to be small.

    \subsection{Example: a Monte Carlo approximation of Pi}

    We begin by defining some notation
    \begin{definition}[Iverson bracket]
        We define the \textbf{Iverson bracket}, for a predicate \(\theta\), to be given by
        \begin{equation}
            \mbb{1}[\theta] = \left\{\begin{array}{ll}
                                         1 & \text{if} \ \phi \\
                                         0 & \text{otherwise}
            \end{array}\right.
        \end{equation}
    \end{definition}

    \TODO{this}

    \subsection{Monte Carlo integration vs. grid-based methods}

    Numerical solutions to (reasonable) 1-dimensional integrals are fast. Simpson's rule, for example, has a convergence rate of \(\mc{O}(1/n^4)\) for 1D functions. SMC, on the other, hand, is not particularly efficient for computing 1D integrals, having error proportional to \(\mc{O}(1/\sqrt{N})\). This error, however, is \textit{independent of the dimension}. Generally, then, SMC methods are better at handling high-dimensional problems. Another advantage is that it does not need any smoothness assumptions.

    \section{Variance Reduction}

    \subsection{Control variates}

    \TODO{this}

    \subsubsection{Example: control variate for SMC estimator of Pi}

    \subsection{Sampling from distributions}

    \TODO{this}

    \section{Rejection sampling}

    \TODO{this}

    \section{Importance sampling}

    \TODO{this}

    \section{Conditioning and posterior inference in Anglican}

    \TODO{this}


\end{document}