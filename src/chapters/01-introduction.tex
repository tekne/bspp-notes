%! Author = marcelroed
%! Date = 11/19/20

% Preamble
\documentclass[../main.tex]{subfiles}

% Document
\begin{document}


    \chapter{Introduction}

    \section{The Bayesian Paradigm}

    There are two major differing views on probability: the \textbf{Frequentist} school, for whom the probability of an event represents the \textit{long-run frequency} of an event over a large number of repetitions of an experiment, and the \textbf{Bayesian} school, who hold that the probability of an event represents a degree of belief about the event. This latter point of view allows us to tackle hypothetical questions, like ``will the Arctic ice cap have disappeared by the end of the century?,'' in terms of beliefs (since, unfortunately, we don't seem to have a large supply of do-overs to find out).

    These lead to two different views on statistics: from the frequentist point of view, the underlying parameters describing a system \(\theta\) are fixed, and the data \(\mc{D}\) are some repeatable random sample performed on the system, the parameters remaining the same every repetition. Think different observations of, or samples from, the same world. On the other hand, from the Bayesian point of view, the \textit{data} \(\mc{D}\) are fixed, and the parameters \(\theta\) are unknown and hence described probabilistically: repetition here adds knowledge about the parameters.

    As an example, the likelihood function \(\Pr[\mc{D} | \theta]\), ``how likely is \(\mc{D}\) in a world \(\theta\),'' is important in both paradigms, but is used very differently.

    \subsection{Probability Basics Review}

    Fix a \textbf{probability space} \((\Omega, \mc{F}, \mc{P})\), and consider random variables (r.v.) \(X\) and \(Y\) taking values, respectively,
    \begin{equation}
        \{x_i: i \in \mc{I}\}, \qquad \{y_i: j \in \mc{J}\}
    \end{equation}
    Recall that we write the \textbf{joint probability} of \(X = x_i\), \(Y = y_i\) as
    \begin{equation}
        \Pr[X = x_i, Y = y_j]
    \end{equation}
    and the \textbf{conditional probability} of \(Y = y_j\) given \(X = x_i\) as
    \begin{equation}
        \Pr[X = x_i | Y = y_j]
    \end{equation}
    This allows us to define the \textbf{marginal probability} of \(X = x_i\), \(\Pr[X = x_i]\), (because it is obtained by marginalizing, i.e. summing out, the other variable) via the \textbf{sum rule}
    \begin{equation}
        \Pr[X = x_i] = \sum_{j \in \mc{J}}\Pr[X = x_i, Y = y_j]
    \end{equation}
    We may then define conditional probability via the \textbf{product rule}
    \begin{equation}
        \Pr[X = x_i, Y = y_j] = \Pr[Y = y_j | X = x_i] \cdot \Pr[X = x_i]
    \end{equation}

    Cox \cite{cox} showed that, as in the Bayesian paradigm, if we take numerical values to represent degrees of belief (\textbf{belief functions}), a simple set of axioms encoding common sense properties of such beliefs yields a set of rules for manipulating degrees of beliefs equivalent to the sum and product rules above, which we often write compactly as
    \begin{equation}
        \Pr[X] = \sum_Y\Pr[X|Y], \qquad \Pr[X, Y] = \Pr[Y | X]\Pr[X]
    \end{equation}

    \subsection{Bayes' Rule}

    Combining the product rule and symmetry \(\Pr[\theta, \mc{D}] = \Pr[\mc{D}, \theta]\), we can directly obtain \textbf{Bayes' Rule}
    \begin{equation}
        \Pr[\theta | \mc{D}] = \frac{\Pr[\mc{D} | \theta]\Pr[\theta]}{\Pr[\mc{D}]}
    \end{equation}

    Breaking this down in terms of beliefs,
    \begin{itemize}

        \item \(\Pr[\theta]\) is the \textbf{prior probability} representing our initial belief about the \textbf{latent variables} (or \textbf{model parameters}) \(\theta\).

        \item \(\Pr[\mc{D} | \theta]\) is the \textbf{likelihood function} (\textit{not} a probability distribution over \(\theta\), though!) expressing how probable the observed data \(\mc{D}\) is with respect to a given setting of \(\theta\).

        \item \(\Pr[\theta | \mc{D}]\) is the \textbf{posterior probability} representing our belief about \(\theta\) after having learned about \(\mc{D}\).

        \item \begin{equation}
                  \Pr(\mc{D}) = \sum_\theta\Pr[D | \theta]\Pr[\theta] = \int\Pr[D|\theta]\Pr[\theta]d\theta
        \end{equation}
        is the \textbf{normalizing constant}, which poses a major computational challenge in machine learning.

    \end{itemize}
    While mathematically trivial, Bayes' rule is foundational for machine learning, as it describes how our prior belief about \(\theta\) is modified by observing \(\mc{D}\); in other words, what we learned.

    \subsubsection{Bayes' rule for density functions}

    Let \((S, \mc{A}, \mu)\) be a probability space and \((\mc{X}, \Sigma_{\mc{X}})\), \((\Omega, \Sigma_{\Omega})\) be Borel spaces. Let \(X: S \to \mc{X}\) and \(\Theta: S \to \Omega\) be random variables, naming \(\Theta\) the \textbf{parameter} and hence making \(\Omega\) the \textbf{parameter space}. Write \(\mu_\Theta\) for the probability measure on \((\Omega, \Sigma_\Omega)\) induced by \(\Theta\) from \(\mu\). This is called the \textbf{prior distribution}, which we can write explicitly as
    \begin{equation}
        \forall A \in \Sigma_{\Omega}, \mu_\Theta(A) = \mu(\Theta^{-1}(A)) \in [0, 1]
    \end{equation}
    We may now, for each \(\theta \in \Omega\), \(A \in \Sigma_{\mc{X}}\), write
    \begin{equation}
        P_\theta(A) = \Pr[X \in A | \Theta = \theta]
    \end{equation}
    We call
    \begin{equation}
        \{P_\theta\}_{\theta \in \Omega}
    \end{equation}
    a \textbf{parametric family of distributions on \(X\)}. We assume that, for each \(\theta \in \Omega\), \(P_\theta \ll \nu\), a \(\sigma\)-finite measure on \(V\).

    \TODO{Conditional density}

    \begin{theorem}[Bayes' rule for density functions]
        Let \(\mu_\Theta(\cdot | x)\), the \textbf{posterior distribution}, be the conditional distribution of \(\Theta\) given \(X = x\). Then
        \begin{enumerate}
            \item \(\mu_{\Theta}|_X(\cdot | x) \ll \mu_\Theta\), \(\mu_X\)-almost surely
            \item Let \(\Xi\) consist of \(x \in \mc{X}\) such that the \textbf{marginal likelihood}
            \begin{equation}
                \int_\Omega f_{X | \Theta}(x | t)\mu_\Theta(dt) = 0 \ \text{or} \ \infty
            \end{equation}
            For \(x \in X \setminus \Xi\),
            \begin{equation}
                \drv{\mu_{\Theta | X}}{\mu_\Theta}(\theta | X) = \frac{f_{X | \Theta}(x | \theta)}{\int_\Omega f_{X | \Theta}(x | t)\mu_\Theta(dt)}
            \end{equation}
            \item \(\Xi\) is \(\mu_X\)-null, that is, the normalizing constant (marginal likelihood) is almost-surely finite.
        \end{enumerate}
    \end{theorem}

    \TODO{Notation}

    \subsubsection{Example: a WhatsApp puzzle}

    Consider the following problem:
    \begin{enumerate}

        \item I've forgotten what day it is.

        \item I receive: \begin{itemize}

                             \item on Sundays, an average of 3 messages per hour

                             \item on other days, an average of 10 messages per hour

        \end{itemize}

        \item I received 4 messages in a given hour

        \item Is it Sunday?

    \end{enumerate}
    Let's work through solving this by an application of Bayes' rule. We begin by identifying random variables \(Day \in \{\mt{Sun}, \mt{nonSun}\}\) (\(\theta\)) and \(Rate \in \nats\) (\(\mc{D})\)). Note that the rates given above are ``frequentist,'' i.e. given a fixed day \(\theta\) we have an average observation \(\mc{D}\), whereas the problem statement is Bayesian, i.e. given fixed data \(\mc{D}\) we must formulate a belief about \(\theta\).

    Recall the \textbf{Bernoulli distribution} \(\Bern(p)\) represents a biased coin flip, giving 1 (``heads'') with probability \(p\) and hence 0 (``tails'') with probability \(1 - p\). Recall also that the \textbf{Poisson distribution}, \(\Poi(\lambda)\), gives the probability of a given number of events happening in a fixed time interval where \(\lambda\) is the average number of events happening in the interval. The probability mass function of \(\Poi(\lambda)\) is given by
    \begin{equation}
        \forall k \in \nats, \Poi(\lambda | k) = \frac{\lambda^k}{e^\lambda k!}
    \end{equation}

    Using this, we can apply Bayes' rule as follows:
    \begin{multline}
        \Pr[Day = \mt{Sun} | Rate = 4]
        = \frac
        {\Pr[Day = \mt{Sun}] \cdot \Pr[Rate = 4 | Date = \mt{Sun}]}
        {\sum_d\Pr[Rate = 4 | Day = d] \cdot \Pr[Day = d]}
        \\ = \frac
        {(1 / 7) \cdot \Poi(4 | 3)}
        {(1 / 7) \cdot \Poi(4 | 3) + (6 / 7) \cdot \Poi(4 | 10)}
        \approx \frac{0.168}{0.168 + 0.114} \approx 0.597
    \end{multline}

    \subsection{Iterating Bayes' rule: using the posterior as the prior}

    Fixing \(\mc{D}\) and viewing all quantities as functions of \(\theta\), Bayes' rule tells us that we have
    \begin{equation}
        \text{Posterior} \propto \text{Likelihood} \times \text{Prior}
    \end{equation}
    A key observation is that Bayes' rule is self-similar under multiple observations. Namely, we can iterate applications of Bayes' rule by using the posterior after the first observation as the prior when we consider the next observation. For example, for observations \(\mc{D}_1\), \(\mc{D}_2\), we can apply Bayes' rule on \(\theta, \mc{D}_2\) to obtain:
    \begin{equation}
        \Pr[\theta | \mc{D}_2, \mc{D}_1]
        = \frac
        {\Pr[\mc{D}_2 | \theta, \mc{D}_1]\Pr[\theta | \mc{D}_1]}
        {\Pr[\mc{D}_2 | \mc{D}_1]}
    \end{equation}
    We may then apply Bayes' rule on \(\theta, \mc{D}_1\) on the above to obtain
    \begin{equation}
        \Pr[\theta | \mc{D}_2, \mc{D}_1]
        = \frac
        {\Pr[\mc{D}_2 | \theta, \mc{D}_1]\Pr[D_1 | \theta]\Pr[\theta]}
        {\Pr[\mc{D}_2 | \mc{D}_1]\Pr[\mc{D}_1]}
    \end{equation}
    Two applications of the product rule therefore yields
    \begin{equation}
        \Pr[\theta | \mc{D}_2, \mc{D}_1]
        = \frac
        {\Pr[\mc{D}_2, \mc{D}_1 | \theta]\Pr[\theta]}
        {\Pr[\mc{D}_2, \mc{D}_1]}
    \end{equation}
    In other words, we may update our beliefs continuously as we make more observations.

    \subsubsection{Example: coin tossing and Bayesian learning}

    \label{sssec:cointoss}

    As an example of the above, consider the following problem: if we toss a \(\theta\)-biased coin \(n\) times and obtain \(x\) heads, estimate \(\theta\).
    Here, \(\theta\) is the \textbf{latent variable} (or \textbf{parameter}), and \(X = x\) is the \textbf{observable variable}. The Bayesian approach to estimating \(\theta\) is as follows:
    \begin{itemize}

        \item We at first assume we know nothing about \(\theta\), so
        \begin{equation}
            \theta \sim \Unif(0, 1)
        \end{equation}
        i.e. we have \textbf{prior pdf} \(p(\theta) = 1\).

        \item We have \textbf{likelihood}, fixing \(\theta\)
        \begin{equation}
            p(x | \theta)
            = { \binom{n}{x} }\theta^x(1 - \theta)^{n - x}
            = \Bin(x | n, \theta)
        \end{equation}
        i.e. the pmf of the \textbf{binomial distribution} \(\Bin(n, \theta)\).

        \item We therefore obtain \textbf{posterior pdf}, using Bayes' rule,
        \begin{multline}
            p(\theta | x)
            = \frac{p(x | \theta)p(\theta)}{p(x)}
            = \frac{p(x | \theta)p(\theta)}{\int_0^1p(x | \theta)p(\theta)d\theta}
            \\ = \frac{\theta^x(1 - \theta)^{n - x}}{\int_0^1\theta^x(1 - \theta)^{n - x}d\theta}
            = \frac{\theta^x(1 - \theta)^{n - x}}{\Beta(1 + x, 1 + n - x)}
        \end{multline}
        which is the pdf of the \textbf{beta distribution} \(\Beta(1 + x, 1 + n - x)\), namely
        \begin{equation}
            \Beta(x | 1 + x, 1 + n - x)
        \end{equation}

    \end{itemize}

    \subsection{Beta and gamma distributions}

    For reference, we define the following distributions:
    \begin{definition}[Beta distribution]
        We define the \textbf{beta distribution} \(\Beta(\alpha, \beta)\) with parameters \(\alpha, \beta \in \reals^+\) with the pdf
        \begin{equation}
            \Beta(x | \alpha, \beta) = \left\{\begin{array}{cc}
                                                  \frac{x^{\alpha - 1}(1 - x)^{\beta - 1}}{\Beta(\alpha, \beta)}
                                                  & \text{if} \ 0 < x < 1 \\
                                                  0 & \text{otherwise}
            \end{array}\right.
        \end{equation}
    \end{definition}
    \noindent
    The beta distribution has the following properties:
    \begin{equation}
        \text{mean} = \frac{\alpha}{\alpha + b},
        \qquad \text{mode} = \frac{\alpha - 1}{\alpha + \beta - 2},
        \qquad \text{variance} = \frac{\alpha\beta}{(\alpha + \beta)^2(\alpha + \beta + 1)}
    \end{equation}
    \begin{definition}[Gamma distribution]
        We define the \textbf{gamma distribution} \(\Gamma(\alpha, \beta)\) with \textbf{shape} \(\alpha \in \reals^+\) and \textbf{rate parameter} \(\beta \in \reals^+\) to have support \((0, \infty)\) and pdf
        \begin{equation}
            \Gamma(x | \alpha, \beta) = \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha - 1}e^{-\beta x}
        \end{equation}
        where \(\Gamma(\alpha) = \int_0^\infty z^{\alpha - 1}e^{-z}dz\) is the \textbf{gamma function}.
    \end{definition}
    \noindent
    Note the gamma function satisfies
    \begin{equation}
        \forall x > 0, \Gamma(x + 1) = x\Gamma(x), \quad \Gamma(1) = 1 \implies \forall \alpha \in \ints^+, \gamma(\alpha) = (\alpha - 1)!
    \end{equation}

    \subsubsection{Aside: conjugate distributions}

    If the posterior distribution \(p(\theta | x)\) is in the same family as the prior distribution \(p(\theta)\), we say that the prior and posterior are \textbf{conjugate distributions}, with the prior being a \textbf{conjugate prior} for the likelihood function \(p(x | \theta)\). Since conjugacy implies that a given prior-likelihood combination yields a posterior with the same form as the prior, it implies a closed-form solution to the inference problem. For example,
    \begin{itemize}

        \item Beta priors are conjugate to binomial likelihoods, as we have in particular
        \begin{equation}
            p(\theta) = \Beta(\theta | \alpha, \beta), p(x | \theta) = \Bin(x| n, \theta) \implies p(\theta | x) = \Beta(\theta | \alpha + x, \beta + n - x)
        \end{equation}

        \item The normal distribution is \textbf{self-conjugate}, i.e. if the likelihood function is a normal distribution with known variance, a normal prior yields a normal posterior.

        \item If, on the other hand, the mean of a normal distribution is known and we wish to infer it's variance, the conjugate prior for the precision (the reciprocal of variance) of a normal distribution obeys a gamma distribution.

    \end{itemize}

    \subsubsection{Example: returning to coin tossing}

    In our original coin tossing experiment in \ref{sssec:cointoss}, where \(n\) tosses returned \(x\) heads, we had prior and posterior
    \begin{equation}
        p(\theta) = 1 = \Beta(\theta | 1, 1), \qquad p(\theta | x) = \Beta(\theta | 1 + x, 1 + n - x)
    \end{equation}
    If we perform a follow up experiment of \(m\) tosses, the previous posterior becomes our prior, allowing us to make new observations the same way. Say, for example, we observe \(y\) heads and \(m - y\) tails: then our new posterior pdf will be
    \begin{equation}
        \Beta(\theta | 1 + x + y, 1 + n - x + m - y)
    \end{equation}
    Qualitatively, we note that as we make more tosses and obtain \(t\) tails and \(h\) heads the distribution converges to a narrow ``spike'' around \(\frac{t}{h}\), as in Figure \ref{fig:convbeta}.

    \begin{figure}
        \includegraphics[width=\textwidth]{convbeta.png}
        \caption{
        The beta distribution starts as uniform (\(\Beta(1, 1)\)) and converges to a narrow spike around \(\frac{n + 1}{m + 1}\) for \(\Beta(n, m)\) as \(n + m \to \infty\).
        }
        \label{fig:convbeta}
    \end{figure}

    \subsection{Asymptotic certainty of posterior inference}

    The Bernstein-von Mises Theorem is a result which connects Bayesian and frequentist inference. Informally, it assumes that there is some true probabilistic process that generates observations, as in frequentism, and then studies the quality of Bayesian methods of recovering that process and making uncertainty statements about it. Formally,

    \begin{theorem}[Bernstein-von Mises]
        Assume a data set \(\mc{D}_n\) comprising \(n\) data points was generated from some true parameter \(\theta^*\). Under some regularity conditions, provided \(p(\theta^*) > 0\), we have
        \begin{equation}
            \lim_{n \to \infty}p(\theta | \mc{D}_n) = \delta(\theta - \theta^*)
        \end{equation}
        In the unrealizable case where data is generated from some distribution \(p^*(x)\) which cannot be modeled by any \(\theta\), the posterior will converge to
        \begin{equation}
            \lim_{n \to \infty}p(\theta | \mc{D}_n) = \delta(\theta - \hat\theta)
        \end{equation}
        where \(\hat\theta\) minimises
        \begin{equation}
            \KL(p^*(x) || p(x | \theta))
        \end{equation}
    \end{theorem}
    In the above, \(\delta\) is the Dirac delta function, which is the limit of zero-centered normal distributions as the variance tends to zero. In general, we have that as the size \(n\) of the dataset approaches infinity, the posterior distribution converges to a normal distribution having mean \(\theta^*\) and variance \(\mc{O}(1/n)\).

    This is a very important result in Bayesian statistics, as it tells us that when our model assumptions are correct, we converge with observation to the true parameters, and moreover, the posterior then becomes independent of the prior when we are provided with sufficient data. Perhaps the most important implication of this theorem is that Bayesian inference is \textbf{asymptotically correct} from a frequentist point of view. That is, from a large amount of data, we can use Bayesian inference to make founded statements about estimation and uncertainty from a frequentist point of view.

    \subsection{Asymptotic consensus of posterior inference}

    \begin{theorem}
        Take two Bayesians with different priors \(p_1(\theta)\) and \(p_2(\theta)\), observing the same data \(\mc{D}\). Assuming \(p_1, p_2\) have the same support, as \(n \to \infty\), the posteriors \(p_1(\theta | \mc{D}_n)\) and \(p_2(\theta | \mc{D}_n)\) will converge in total variation distance \(d\) between distributions, where
        \begin{equation}
            d(P_1, P_2) = \sup_E|P_1(E) - P_2(E)|
        \end{equation}
    \end{theorem}

    This theorem gives further evidence that the actual prior distribution we use does not matter in the limit as the size of the dataset approaches infinity.

    \section{Probabilistic Programming}

    Given a system with some data, \textbf{probabilistic machine learning} is the process of
    \begin{enumerate}

        \item Building a probabilistic model capable of generating data observed by the system, and then
        \label{item:pmach1}

        \item Using probability to express belief and uncertainty (including, e.g., noise) about the model. It then remains to

        \item Apply Bayes' rule (this is called \textbf{Bayesian inversion}) to learn from the data. We can, for example, infer unknown quantities, make predictions, and explore different models.
        \label{item:pmach3}

    \end{enumerate}

    Probabilistic model development, stage \ref{item:pmach1}, and the design and implementation of inference algorithms, used in the stage \ref{item:pmach3}, are very time consuming and error prone, often requiring bespoke constructions. A possible solution is \textbf{probabilistic programming}. Probabilistic programming is a general-purpose means of expressing probabilistic models as preograms and automatically performing Bayesian inference. This offers an elegant way to generalize \textbf{graphical models} allowing a much richer representation of models, \textit{compositionally}.

    A \textbf{probabilistic programming language} is a deterministic programming language \textit{augmented} with three probabilistic constructs: \textbf{sample} (corresponding to the \textbf{prior}), \textbf{observe} (corresponding to the \textbf{likelihood}), and \textbf{normalize} (corresponding to the \textbf{posterior}), which correspond to the three terms of Bayes' rule.

    \begin{itemize}

        \item \textbf{sample} draws samples from a probability distribution

        \item \textbf{observe}, given a data point, observes it's likelihood

        \item \textbf{normalize} computes the normalising constant, and therefore, by Bayes' rule, the posterior probability.

        This last construct is an \textit{idealized} construct, which is not directly implemented, unlike the former two, which are much more straightforward. Rather, probabilistic programming systems have \textbf{inference engines} which allow computing approximations of the normalizing constant.

    \end{itemize}


    The purpose of a \textbf{probabilistic program}, that is, a program written in a probabilistic programming language, is to \textit{implicitly} specify a probability distribution, the posterior. \textbf{Bayesian inference}, is the problem of computing an explicit representation of, and answering queries about, the distribution which a probabilistic program specifies.
    However, what a probabilistic program \textit{actually} denotes is the product of the prior and likelihood, which is \textit{not} in general a probability measure. The \textbf{inference problem} is the problem of actually finding the normalizing constant so the posterior probability may be computed.

    The title of this course is \textbf{Bayesian Statistical Probabilistic Programming}, \textbf{BSPP}, which is quite a mouthful. Is BSPP just probabilistic programming? Yes, as by probabilistic programming, people mostly mean BSPP. Confusingly, however, probabilistic programming can mean something else, in fact something less than BSPP. Probabilistic programming, also known as \textbf{randomized programming}, relates to programs which incorporate a degree of randomness in their implementation. By statistical probabilistic programming, we emphasize that these programs are equipped with conditional probability constructs, such as observe. By Bayesian, we underline the fact that probabilistic programming is a systematic and principled way to do Bayesian machine learning.

    \subsection{Example: WhatsApp Puzzle Revisited}

    Let's return to the WhatsApp puzzle, which we can solve by probabilistic programming. First, we formulate the puzzle as a program which describes the posterior probability, that is, the probability that it is Sunday, given the data, that is, the receipt of four messages in an hour. Recall the problem description:
    \begin{enumerate}
        \item I've forgotten what day it is
        \item If it's Sunday, I receive 3 WhatsApp messages an hour on average, otherwise, I receive 10
        \item I received 4 messages in a given hour
        \item Is it Sunday?
    \end{enumerate}
    Here is some functional pseudocode which encodes the problem:
    \begin{lstlisting}[style=pplstyle]
    normalize(
        let sunday = sample(bernoulli(1/7)) in
        let rate = if sunday then 3 else 10 in
        observe 4 from poisson(rate);
        return(sunday)
    )
    \end{lstlisting}
    Here, the definition of ``\(\mt{sunday}\)'' (on line 2) encodes the fact that we forgot what day it is, so we assume there is a 1/7 probability of it being any given weekday, and hence a 1/7 probability of it being Sunday and a 6/7 probability of it not being Sunday. This, of course, can be simulated by taking a \textbf{sample} from the Bernoulli distribution with \(p = 1/7\). Computing the rate from our random variable ``sunday'', we we then \textbf{observe} (on line 4) the rate, 4, from the probability distribution the rate obeys, the Poisson distribution with probability ``\(\mt{rate}\).''

    Having formulated the problem as a functional program, the simplest way to sample from the distribution is just to run the program multiple times using a suitable \textbf{Monte Carlo sampling algorithm}. Other methods, of course, are possible.

    \subsection{Promises and Challenges of Probabilistic Programming}

    An important feature of probabilistic programming is \textbf{separation of concerns}: data scientists and domain experts can focus on designing good models, which is what they are best at doing, and leave the development of efficient inference engines to experts in Bayesian statistics, machine learning, and programming languages. The ultimate goal is to democratize access to machine learning.

    The full potential of probabilistic programming will come from \textit{automating} the inference of the unobserved, or \textbf{latent}, variables in the model conditioned on the observed data. Probabilistic programming could be revolutionary for AI and scientific modeling in many ways. Firstly, the universal inference engine obviates the need to manually derive and implement inference methods for models. Secondly, probabilistic programming promises to allow rapid prototyping and testing of different models on the data. Thirdly, probabilistic programming, by promoting separation between the model and inference procedures, encourages model-based thinking. Because of their generality and their universality, probabilistic programming poses very interesting and challenging research problems for the foundations of both statistics and programming languages.


\end{document}